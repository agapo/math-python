TD git

Partie 1 : Commandes avancées Git

1. Clonage et Fork du Projet : 
J'ai commencé par cloner le projet math-python en utilisant la commande git clone https://gitlab.univ-lr.fr/abellion/math-python.git. Ensuite, j'ai forké le dépôt pour avoir ma propre copie sur laquelle travailler. J'ai configuré un nouveau remote pour mon fork que j'ai nommé math. Cela me permet de distinguer clairement le dépôt original de ma version personnelle.

2. Identification et Correction du Bogue : 
Après avoir examiné les fichiers du projet, j'ai lancé les tests unitaires avec pytest. J'ai remarqué que sur les 10 tests, un seul ne passait pas. En examinant les résultats des tests et le code source, j'ai identifié le test de la fonction Fibonacci comme étant le fautif. J'ai découvert que le problème venait d'une erreur dans le nom de la fonction testée. Après avoir corrigé le nom, j'ai exécuté à nouveau les tests pour m'assurer que tous passaient.

3. Création d'une Branche de Correction : 
Pour travailler sur la correction, j'ai créé une branche nommée math.

4. Commits Progressifs : 
J'ai effectué des commits progressifs au fur et à mesure de mes modifications.

5. Réorganisation des Commits avec Git Rebase
Après avoir terminé la correction, j'ai utilisé git rebase -i HEAD~ pour réorganiser mes commits. J'ai choisi de fusionner des commits similaires avec l'option squash pour garder l'historique propre et compréhensible.

6. Utilisation de Git Bisect : 
Pour identifier le commit à l'origine du bogue, j'ai utilisé git bisect. 

7. Identification du Développeur avec Git Blame : 
J'ai ensuite utilisé git blame sur le fichier de test pour identifier qui avait introduit l'erreur. L'objectif était de comprendre le contexte du bogue.

8. Application de la Correction avec Git Cherry-Pick : 
Pour appliquer ma correction à la branche production, j'ai utilisé git cherry-pick. Cela m'a permis de sélectionner uniquement les commits pertinents de ma branche de correction et de les appliquer sur production.

9. Mise à Jour de la Branche avec Git Merge : 
Pour rester synchronisé avec les derniers changements de la branche principale, j'ai utilisé git merge test-gapo dans ma branche main.

10. Création d'un Patch avec Git Format-Patch : 
Enfin, j'ai créé un patch de ma correction avec git format-patch. Ce fichier représente les modifications que j'ai apportées et peut être utilisé pour partager la correction avec d'autres développeurs ou pour l'intégrer dans d'autres branches ou forks du projet.

Partie 2 : Collaboration de code avec GitLab

Partie 3 : Utilisation de pre-commit pour Python
